    <!-- Page Footer-->
    <footer class="section section-fluid footer-classic nodrop safe-mode">
        <div class="container-fluid bg-image-1">
            <div class="row justify-content-center edit" id="footer" rel="global" field="footer-site">
                <div class="col-md-10 col-lg-12 col-xl-4 wow fadeInRight">
                    <div class="box-footer box-footer-small">
                        <module type="logo" name="footer-logo" class="footer-brand" id="footer-logo"  />
                        <p class="text-width-medium">Suntem un furnizor de servicii de construcții lider și premiat în industrie, gata să lucreze la un proiect de orice complexitate, indiferent dacă este comercial sau rezidențial.</p>
                        <div class="contact-classic">
                            <div class="contact-classic-item">
                                <div class="unit align-items-center">
                                    <div class="unit-left">
                                        <h6 class="contact-classic-title">Adresa</h6>
                                    </div>
                                    <div class="unit-body contact-classic-link"><a href="#">str. Traian 63, Brasov, 500334</a></div>
                                </div>
                            </div>
                            <div class="contact-classic-item">
                                <div class="unit align-items-center">
                                    <div class="unit-left">
                                        <h6 class="contact-classic-title">Telefon</h6>
                                    </div>
                                    <div class="unit-body contact-classic-link"><a href="tel:#">+40 748 123-456</a>, <a href="tel:#"> +40 749 123-456</a>
                                    </div>
                                </div>
                            </div>
                            <div class="contact-classic-item">
                                <div class="unit align-items-center">
                                    <div class="unit-left">
                                        <h6 class="contact-classic-title">E-mails</h6>
                                    </div>
                                    <div class="unit-body contact-classic-link"><a href="mailto:#"> info@<?php echo $_SERVER['SERVER_NAME'] ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <module type="social_links" template="footer" name="footer_social_links" class="list-inline list-inline-sm footer-social-list" id="footer_social_links" />
                              
                    </div>
                </div>
                <div class="col-md-10 col-lg-6 col-xl-4 wow fadeInRight" data-wow-delay=".1s">
                    <div class="box-footer">
                        
                        <module type="contact_form" class="rd-form rd-mailform" name="footer_form"  id="footer-form" />
                        
                        
                    </div>
                    
                </div>
                <div class="col-md-10 col-lg-6 col-xl-4 wow fadeInRight" data-wow-delay=".2s">
                    <module type="content" template="footer" name="footer--nav" parent="45" />
                </div>
            </div>
        </div>
        <div class="container footer-bottom-panel wow fadeInUp">
            <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span><span>&nbsp;</span><span><?php echo $_SERVER['SERVER_NAME'] ?></span>. Toate drepturile rezervate. <a href="#">Politica de confidentialitate</a>
            </p>
        </div>
    </footer>    
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="<?php print TEMPLATE_URL; ?>js/core.min.js"></script>
    <script src="<?php print TEMPLATE_URL; ?>js/script.js"></script>
</body>
</html>