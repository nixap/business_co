<?php
/*

  type: layout
  content_type: static
  name: Contact Us

  description: Contact us layout
  position: 7
 */
?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<module type="breadcrumb" />
<div id="content">
    <div class="edit" field="content" rel="content">

        <div class="breadcrumbs-custom-inset">
            <section class="section section-sm section-first bg-default">
                <div class="container">
                    <div class="row row-30 justify-content-center">
                        <div class="col-sm-8 col-md-6 col-lg-4">
                            <article class="box-contacts">
                                <div class="box-contacts-body">
                                    <div class="box-contacts-icon fl-bigmug-line-cellphone55"></div>
                                    <div class="box-contacts-decor"></div>
                                    <p class="box-contacts-link"><a href="tel:#">+40 723-913-468</a></p>
                                    <p class="box-contacts-link"><a href="tel:#">+40 723-888-455</a></p>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-8 col-md-6 col-lg-4">
                            <article class="box-contacts">
                                <div class="box-contacts-body">
                                    <div class="box-contacts-icon fl-bigmug-line-up104"></div>
                                    <div class="box-contacts-decor"></div>
                                    <p class="box-contacts-link"><a href="#">Traian 63, 500334, Brasov</a></p>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-8 col-md-6 col-lg-4">
                            <article class="box-contacts">
                                <div class="box-contacts-body">
                                    <div class="box-contacts-icon fl-bigmug-line-chat55"></div>
                                    <div class="box-contacts-decor"></div>
                                    <p class="box-contacts-link"><a href="mailto:#">mail@<?php echo $_SERVER['SERVER_NAME'] ?></a></p>
                                    <p class="box-contacts-link"><a href="mailto:#">info@<?php echo $_SERVER['SERVER_NAME'] ?></a></p>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>


            <div class="mw-row">
                <div class="mw-col" style="width: 50%;">
                    <div class="mw-col-container">
                        <module type="contact_form" template="dark" class="contact-form" id="contact-form" />
                    </div>
                </div>
                <div class="mw-col" style="width: 50%;">
                    <div class="mw-col-container text-left" style="padding-left: 40px;">
                        <module type="google_maps" />
                    </div>
                </div>

            </div>



        </div>
    </div>
</div>
<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
