<?php

/*

type: layout
content_type: dynamic
name: Blog
position: 3
description: Blog

*/


?>
<?php include THIS_TEMPLATE_DIR. "header.php"; ?>

<div id="content">
<module type="breadcrumb" />
    
      <section class="section section-xl bg-default text-md-left">
        <div class="container" id="blog-container">
            <div class="row row-60">
                <module data-type="posts" template="blog" data-page-id="<?php print CONTENT_ID ?>"  />
                
            </div>
        </div>
      </section>
	
</div>
<?php include THIS_TEMPLATE_DIR. "footer.php"; ?>
