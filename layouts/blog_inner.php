
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<module type="breadcrumb" />

<section class="section section-xl bg-default">
    <div class="container">

        <div class="row row-70">
            <div class="col-lg-8">
                <div class="single-service">
                    <h3 class="edit" field="title" rel="content">Titlul paginii</h3>
                    <div class="text-spacing-100 edit"  field="content" rel="content">
                        <p>
                            Domuss cadunt, tanquam barbatus messor. Sunt fluctuses quaestio domesticus, placidus rationees. Cum deus observare, omnes nutrixes imitari castus, festus messores. Cum torus studere, omnes resistentiaes imperium domesticus, emeritis mortemes.
                        </p>
                    </div>
                    <p>&nbsp;</p>
                    <div class="edit" rel="content" field="comments"><module data-type="comments" data-template="default" data-content-id="<?php print CONTENT_ID; ?>"  /></div>

                </div>
                
            </div>
            <div class="col-lg-4">
                <!-- Post Sidebar-->
                <div class="post-sidebar post-sidebar-inset">
                    <div class="row row-lg row-60">
                        <div class="col-sm-6 col-lg-12">
                            <module type="pages" data-template="sidebar" name="sidebar_services" data-parent="45" />
                        </div>
                        <div class="col-sm-6 col-lg-12 nodrop safe-mode edit"  field="right-contacts" rel="content">
                            <div class="post-sidebar-item">
                                <h5>Ținem legătura</h5>
                                <div class="post-sidebar-item-inset">
                                    <ul class="list-contacts">
                                        <li>
                                            <div class="unit unit-spacing-xs">
                                                <div class="unit-left"><span class="icon fa fa-phone"></span></div>
                                                <div class="unit-body"><a class="link-phone" href="tel:#">+40 723-913-468</a></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="unit unit-spacing-xs">
                                                <div class="unit-left"><span class="icon fa fa-envelope"></span></div>
                                                <div class="unit-body"><a class="link-email" href="mailto:#">info@<?php echo $_SERVER['SERVER_NAME'] ?></a></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="unit unit-spacing-xs">
                                                <div class="unit-left"><span class="icon fa fa-location-arrow"></span></div>
                                                <div class="unit-body"><a class="link-location" href="#">Brașov, str. Traian 63</a></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
