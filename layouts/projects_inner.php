<?php
/*

  type: layout
content_type: dynamic
  name: Proiecte

  description: Proiecte
  position: 04
 */


$page = get_content_by_id(PAGE_ID);
?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<module type="breadcrumb" />


<section class="section bg-default edit" field="content" rel="project-<?php print CONTENT_ID; ?>">
    <div class="container">
        <module type="pictures" template="simple" data-parent="<?php print CONTENT_ID; ?>" />
    </div>
</section>

<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
