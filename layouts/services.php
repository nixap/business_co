<?php
/*

  type: layout
  content_type: static
  name: Servicii

  description: Servicii
  position: 01
 */
?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<module type="breadcrumb" />


    <module type="content" template="services" parent="45" />

<section class="section bg-default edit" field="content" rel="page">
    <div class="container">
        
    </div>
</section>
    
    <module type="content" rel="services-projects" data-parent="49" template="projects"  />
    <module type="layouts" rel="home-about" template="counters" name="about_module" />

<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
