<?php
/*

  type: layout
content_type: dynamic
  name: Proiecte

  description: Proiecte
  position: 04
 */


$page = get_content_by_id(PAGE_ID);
?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<module type="breadcrumb" />

    <module type="posts" data-parent="49" template="projects_clean"  />


<section class="section bg-default edit" field="content" rel="project-<?php echo $page['id']; ?>">
    <div class="container" id="blog-container">
        
    </div>
</section>

<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
