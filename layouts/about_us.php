<?php

/*

type: layout
content_type: static
name: About us

description: About us layout
position: 6
*/


?>
<?php include THIS_TEMPLATE_DIR. "header.php"; ?>

<module type="breadcrumb" />
<div class="container edit our-team">
	<div id="about-us-content" class="edit" field="content" rel="content">
		<div class="clear"></div>
		<p class="element">&nbsp;</p>
		<div class="clear"></div>
		<h3 class="intro element" align="center"> <strong style="font-weight: 500;">For a better view of your company, brand or website add some pictures or stories of your team.</strong><br />
			
		</h3>
		<p class="element">&nbsp;</p>
		<div class="mw-row">
			<div class="mw-col" style="width:50%">
				<div class="mw-col-container">
					<div class="element" style="width:95%">
						<p align="justify">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
					</div>
				</div>
			</div>
			<div class="mw-col" style="width:50%">
				<div class="mw-col-container">
					<div class="element" style="width:95%">
						<p align="justify">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <module type="layouts" rel="home-about" template="counters" name="about_module" />
	<div class="clear"></div>
</div>
<?php include THIS_TEMPLATE_DIR. "footer.php"; ?>
