<?php
/*

  type: layout

  name: Sidebar servicii

  description: List Navigation

 */
?>

<?php
$params['ul_class'] = 'list list-categories';
$params['ul_class_deep'] = 'nav nav-pills nav-stacked';
?>

<div class="post-sidebar-item">
    <h5>Servicii</h5>
    <div class="post-sidebar-item-inset inset-right-20">
        <?php pages_tree($params); ?>
    </div>
</div>
