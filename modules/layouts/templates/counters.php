<?php
/*

  type: layout

  name: Home about

  description: Home about

 */
?>
<?php
print lnotif('Click here to select layout');
?>


<section class="section section-sm bg-default">
    <div class="container position-relative nodrop safe-mode edit" rel="content" field="home-c-content">
        <div class="img-decorative"><img src="<?php print TEMPLATE_URL; ?>images/img-decorative-720x580.png" alt="" width="720" height="580"/>
        </div>
        <div class="row row-50 row-xl-24 justify-content-center align-items-center align-items-lg-start text-left">
            <div class="col-md-6 col-lg-5 col-xl-4 text-center"><a class="text-img custom-offset-bottom" href="about-us.html"><span class="counter">20</span></a></div>
            <div class="col-sm-8 col-md-6 col-lg-5 col-xl-4">
                <div class="text-width-extra-small inset-md-left-30 offset-top-lg-24 wow fadeInUp">
                    <h3 class="title-decoration-lines-left">Ani de experiență</h3>
                    <p class="text-gray-500">Dezvoltăm obiecte care ne fac să ne mândrim.</p><a class="button button-primary button-pipaluk" href="/about">Despre noi</a>
                </div>
            </div>
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-4 wow fadeInRight" data-wow-delay=".1s">
                <div class="row justify-content-center border-2-column offset-top-xl-26">
                    <div class="col-9 col-sm-6">
                        <div class="counter-amy">
                            <div class="counter-amy-number"><span class="counter">20</span>
                            </div>
                            <h6 class="counter-amy-title">Proiecte</h6>
                        </div>
                    </div>
                    <div class="col-9 col-sm-6">
                        <div class="counter-amy">
                            <div class="counter-amy-number"><span class="counter">40</span>
                            </div>
                            <h6 class="counter-amy-title">Parteneri</h6>
                        </div>
                    </div>
                    <div class="col-9 col-sm-6">
                        <div class="counter-amy">
                            <div class="counter-amy-number"><span class="counter">120</span>
                            </div>
                            <h6 class="counter-amy-title">Profesioniști</h6>
                        </div>
                    </div>
                    <div class="col-9 col-sm-6">
                        <div class="counter-amy">
                            <div class="counter-amy-number"><span class="counter">260</span>
                            </div>
                            <h6 class="counter-amy-title">Tone de materiale</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-12 align-self-center ">
                <div class="row row-30 justify-content-center safe-mode">
                    <div class="cloneable col-sm-6 col-md-5 col-lg-6 col-xl-3 wow fadeInLeft"><a class="clients-classic" href="#"><img src="<?php print TEMPLATE_URL; ?>images/clients-9-270x117.png" alt="" width="270" height="117"/></a></div>
                    <div class="cloneable col-sm-6 col-md-5 col-lg-6 col-xl-3 wow fadeInLeft" data-wow-delay=".1s"><a class="clients-classic" href="#"><img src="<?php print TEMPLATE_URL; ?>images/clients-10-270x117.png" alt="" width="270" height="117"/></a></div>
                    <div class="cloneable col-sm-6 col-md-5 col-lg-6 col-xl-3 wow fadeInLeft" data-wow-delay=".2s"><a class="clients-classic" href="#"><img src="<?php print TEMPLATE_URL; ?>images/clients-3-270x117.png" alt="" width="270" height="117"/></a></div>
                    <div class="cloneable col-sm-6 col-md-5 col-lg-6 col-xl-3 wow fadeInLeft" data-wow-delay=".3s"><a class="clients-classic" href="#"><img src="<?php print TEMPLATE_URL; ?>images/clients-11-270x117.png" alt="" width="270" height="117"/></a></div>
                </div>
            </div>
        </div>
    </div>
</section>