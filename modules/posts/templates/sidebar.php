<?php
/*

  type: layout

  name: Services

  description: Services

 */
?>

<?php
$tn = $tn_size;
if (!isset($tn[0]) or ( $tn[0]) == 150) {
    $tn[0] = 220;
}
if (!isset($tn[1])) {
    $tn[1] = $tn[0];
}
?>
<?php
$only_tn = false;


$search_keys = array('title', 'created_at', 'description', 'read_more');

if (isset($show_fields) and is_array($show_fields) and ! empty($show_fields)) {
    $only_tn = true;
    foreach ($search_keys as $search_key) {
        foreach ($show_fields as $show_field) {
            if ($search_key == $show_field) {
                $only_tn = false;
            }
        }
    }
}
?>
<section class="section section-xl bg-default">
    <div class="container">
        <div class="row row-40">
            <?php if (!empty($data)): ?>

                <?php foreach ($data as $item): ?>
            
            <div class="col-md-6 col-lg-4">
                <article class="box-icon-modern">
                    <div class="box-icon-modern-icon linearicons-city"></div>
                    <h5 class="box-icon-modern-title"><a href="<?php print $item['link'] ?>"><?php print $item['title'] ?></a></h5>
                    <div class="box-icon-modern-decor"></div>
                    <p class="box-icon-modern-text"><?php print $item['description'] ?></p>
                </article>
            </div>
                   
                <?php endforeach; ?>

            <?php endif; ?>

        </div>
    </div>
</section>





