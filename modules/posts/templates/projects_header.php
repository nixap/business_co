<?php
/*

  type: layout

  name: Home projects

  description: Projects

 */
?>

<?php
$tn = $tn_size;
if (!isset($tn[0]) or ( $tn[0]) == 150) {
    $tn[0] = 220;
}
if (!isset($tn[1])) {
    $tn[1] = $tn[0];
}
?>
<?php
$only_tn = false;

$search_keys = array('title', 'created_at', 'description', 'read_more');

if (isset($show_fields) and is_array($show_fields) and ! empty($show_fields)) {
    $only_tn = true;
    foreach ($search_keys as $search_key) {
        foreach ($show_fields as $show_field) {
            if ($search_key == $show_field) {
                $only_tn = false;
            }
        }
    }
}
?>




<?php if (!empty($data)): ?>

    <div class="row" data-lightgallery="group">
        <div class="col-12">
            <?php foreach ($data as $item): ?>
            <article class="thumbnail thumbnail-mary">
                <div class="thumbnail-mary-figure"><img src="<?php print thumbnail($item['image'], 330, $tn[1]); ?>" alt="" width="330" height="240"/>
                </div>
                <div class="thumbnail-mary-caption"><a class="icon fl-bigmug-line-zoom60" href="<?php print thumbnail($item['image'], 1920); ?>" data-lightgallery="item"><img src="<?php print thumbnail($item['image'], 330, $tn[1]); ?>" alt="" width="330" height="240"/></a>
                </div>
            </article>
            <?php endforeach; ?>
        </div>
    </div>                     





<?php endif; ?>