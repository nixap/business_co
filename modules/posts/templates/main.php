<?php
/*

  type: layout

  name: Home services

  description: Services

 */
?>

<?php
$tn = $tn_size;
if (!isset($tn[0]) or ( $tn[0]) == 150) {
    $tn[0] = 220;
}
if (!isset($tn[1])) {
    $tn[1] = $tn[0];
}
?>
<?php
$only_tn = false;


$search_keys = array('title', 'created_at', 'description', 'read_more');

if (isset($show_fields) and is_array($show_fields) and ! empty($show_fields)) {
    $only_tn = true;
    foreach ($search_keys as $search_key) {
        foreach ($show_fields as $show_field) {
            if ($search_key == $show_field) {
                $only_tn = false;
            }
        }
    }
}
?>


<section class="section section-sm section-first bg-default text-center">
    <div class="container">
        <div class="row row-30 justify-content-center">
            <div class="col-md-7 col-lg-5 col-xl-6 text-lg-left wow fadeInUp">
                <div class="figure-classic figure-classic-pattern figure-classic-left edit" rel="content" field="home_services">
                    <img src="<?php print TEMPLATE_URL; ?>images/index-1-513x561.jpg" alt="" width="513" height="561"/>
                </div>
            </div>
            <div class="col-lg-7 col-xl-6">
                <div class="row row-30">
                    <div class="col-sm-6 wow fadeInRight">
                        <article class="box-icon-modern box-icon-modern-custom nodrop safe-mode edit" rel="content" field="home_services_element">
                            <div>
                                <h3 class="box-icon-modern-big-title">Servicii</h3>
                                <div class="box-icon-modern-decor"></div><a class="button button-md button-default-outline-2 button-wapasha" href="/services">Toate</a>
                            </div>
                        </article>
                    </div>
                    <?php if (!empty($data)): ?>
                    
                    <?php foreach ($data as $item): ?>
                    <div class="col-sm-6 wow fadeInRight" data-wow-delay=".1s">
                        <article class="box-icon-modern box-icon-modern-2">
                            <div class="box-icon-modern-icon linearicons-city"></div>
                            <h5 class="box-icon-modern-title"><a href="<?php print $item['link'] ?>"><?php print $item['title'] ?></a></h5>
                            <div class="box-icon-modern-decor"></div>
                            <p class="box-icon-modern-text"><?php print $item['description'] ?></p>
                        </article>
                    </div>
                    <?php endforeach; ?>
                    
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </div>
</section>





