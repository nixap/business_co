<?php
/*

  type: layout

  name: Home services

  description: Services

 */
?>

<?php
$tn = $tn_size;
if (!isset($tn[0]) or ( $tn[0]) == 150) {
    $tn[0] = 220;
}
if (!isset($tn[1])) {
    $tn[1] = $tn[0];
}
?>
<?php
$only_tn = false;


$search_keys = array('title', 'created_at', 'description', 'read_more');

if (isset($show_fields) and is_array($show_fields) and ! empty($show_fields)) {
    $only_tn = true;
    foreach ($search_keys as $search_key) {
        foreach ($show_fields as $show_field) {
            if ($search_key == $show_field) {
                $only_tn = false;
            }
        }
    }
}
?>


<section class="section section-sm section-last bg-default">
    <div class="container">
        <h3 class="nodrop safe-mode edit" field="blog-title-<?php print $params['id']; ?>" rel="blog-title-<?php print $params['id']; ?>">Recent din blog</h3>
        <div class="row row-40 row-sm-lg">
            <?php foreach ($data as $item): ?>
            <div class="col-sm-6 col-lg-4 wow fadeInLeft">
                <!-- Post Modern-->
                <article class="post post-modern">
                    <a class="post-modern-figure" href="<?php print $item['link'] ?>"><img src="<?php print thumbnail($item['image'], 330, $tn[1]); ?>" alt="" width="370" height="307"/>
                        <div class="post-modern-time">
                            <time datetime="<?php print $item['created_at'] ?>"><span class="post-modern-time-number"></span><span class="post-modern-time-month"><?php print $item['created_at'] ?></span></time>
                        </div>
                    </a>
                    <h5 class="post-modern-title"><a href="<?php print $item['link'] ?>"><?php print $item['title'] ?></a></h5>
                    <p class="post-modern-text"><?php print $item['description'] ?></p>
                </article>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>





