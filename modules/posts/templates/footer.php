<?php
/*

  type: layout

  name: Footer servicii

  description: List Navigation

 */
?>
<?php
$only_tn = false;

$search_keys = array('title', 'created_at', 'description', 'read_more');

if (isset($show_fields) and is_array($show_fields) and ! empty($show_fields)) {
    $only_tn = true;
    foreach ($search_keys as $search_key) {
        foreach ($show_fields as $show_field) {
            if ($search_key == $show_field) {
                $only_tn = false;
            }
        }
    }
}
?>
<div class="box-footer">
        <h3 class="font-weight-normal">Servicii</h3>
        <ul class="footer-list-category">
            <?php foreach ($data as $item): ?>
            <li class="heading-5"><a href="<?php print $item['link'] ?>"><?php print $item['title'] ?><span></span></a></li>
            <?php endforeach;  ?>
        </ul>
    </div>
         