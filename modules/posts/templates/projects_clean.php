<?php
/*

  type: layout

  name: Projects clean

  description: Projects

 */
?>

<?php
$tn = $tn_size;
if (!isset($tn[0]) or ( $tn[0]) == 150) {
    $tn[0] = 220;
}
if (!isset($tn[1])) {
    $tn[1] = $tn[0];
}
?>
<?php
$only_tn = false;

$search_keys = array('title', 'created_at', 'description', 'read_more');

if (isset($show_fields) and is_array($show_fields) and ! empty($show_fields)) {
    $only_tn = true;
    foreach ($search_keys as $search_key) {
        foreach ($show_fields as $show_field) {
            if ($search_key == $show_field) {
                $only_tn = false;
            }
        }
    }
}
?>




<?php if (!empty($data)): ?>

                        


<section class="section section-xl bg-default text-center">
    <div class="container isotope-wrap">
        
        <div class="row row-30 isotope z-1" data-lightgallery="group">
            <?php foreach ($data as $item): ?>
            
            
            <div class="col-md-6 col-lg-3 isotope-item" data-filter="Type 3">
              <!-- Thumbnail Modern-->
              <article class="thumbnail thumbnail-modern">
                  <a class="thumbnail-modern-figure" href="<?php print thumbnail($item['image'], 1920); ?>" data-lightgallery="item">
                      <img src="<?php print thumbnail($item['image'], 330, $tn[1]); ?>" alt="" height="303"/>
                  </a>
                <div class="thumbnail-modern-caption">
                  <h5 class="thumbnail-modern-title"><a href="<?php print $item['link'] ?>"><?php print $item['title'] ?></a></h5>
                  <p class="thumbnail-modern-subtitle"><?php print $item['description'] ?></p>
                </div>
              </article>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>



                    <?php endif; ?>