<?php
/*

  type: layout

  name: Footer

  description: Default

 */
?>



    <?php
    if ($social_links_has_enabled == false) {
        print lnotif('Social links');
    }
    ?>


    <?php if ($facebook_enabled) { ?>
        <li><a class="icon fa fa-facebook" href="//facebook.com/<?php print $facebook_url; ?>" target="_blank"></a></li>
    <?php } ?>
    <?php if ($twitter_enabled) { ?>
    <li><a class="icon fa fa-twitter" href="//twitter.com/<?php print $twitter_url; ?>" target="_blank"></a></li>
    <?php } ?>
    <?php if ($googleplus_enabled) { ?>
    <li><a class="icon fa fa-google-plus" href="//plus.google.com/<?php print $googleplus_url; ?>" target="_blank"></a></li>
    <?php } ?>
    
    <?php if ($pinterest_enabled) { ?>
    <li><a class="icon fa fa-pinterest" href="//pinterest.com/<?php print $pinterest_url; ?>" target="_blank"></a></li>
    <?php } ?>
    
    <?php if ($youtube_enabled) { ?>
    <li><a class="icon fa fa-youtube" href="//youtube.com/<?php print $youtube_url; ?>" target="_blank"></a></li>
    <?php } ?>
    
    <?php if ($instagram_enabled) { ?>
    <li><a class="icon fa fa-instagram" href="https://instagram.com/<?php print $instagram_url; ?>" target="_blank"></a></li>
    <?php } ?>
    <?php if ($linkedin_enabled) { ?>
    <li><a class="icon fa fa-linkedin" href="//linkedin.com/<?php print $linkedin_url; ?>" target="_blank"></a></li>
    <?php } ?>
    
    <?php if ($github_enabled) { ?>
    <li><a class="icon fa fa-github" href="//github.com/<?php print $github_url; ?>" target="_blank"></a></li>
    <?php } ?>
    <?php if ($soundcloud_enabled) { ?>
    <li><a class="icon fa fa-soundcloud" href="//soundcloud.com/<?php print $soundcloud_url; ?>" target="_blank"></a></li>
    <?php } ?>
    <?php if ($mixcloud_enabled) { ?>
    <li><a class="icon fa fa-instagram" href="//mixcloud.com/<?php print $mixcloud_url; ?>" target="_blank"></a></li>
    <?php } ?>
    <?php if ($medium_enabled) { ?>
    <li><a class="icon fa fa-instagram" href="//medium.com/<?php print $medium_url; ?>" target="_blank"></a></li>
    <?php } ?>


