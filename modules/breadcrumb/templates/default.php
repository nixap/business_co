<?php
/*

  type: layout

  name: Default

  description: Default


 */

$page = get_content_by_id(PAGE_ID);
?>


<?php if (isset($data) and is_array($data)): ?>
<section class="breadcrumbs-custom-inset">
    <div class="breadcrumbs-custom context-dark">
        <div class="container">
            <h2 class="breadcrumbs-custom-title" field="title" rel="content"><?php print $page['title']; ?></h2>
            <ul class="breadcrumbs-custom-path">
                <li><a href="<?php print(site_url()); ?>"><?php print _e('Home'); ?></a></li>
                <?php foreach ($data as $item): ?>
                    <?php if (!($item['is_active'])): ?>
                    <li><a href="<?php print($item['url']); ?>"><?php print($item['title']); ?></a></li>
                    <?php else: ?>
                    <li class="active"><?php print($item['title']); ?></li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="box-position" style="background-image: url(<?php print TEMPLATE_URL; ?>images/bg-about.jpg);"></div>
    </div>
</section>
    <?php endif; ?>