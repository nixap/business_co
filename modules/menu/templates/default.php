<?php

/*

type: layout

name: Main Menu

description: Default Menu skin

*/

  $menu_filter['ul_class'] = 'rd-navbar-nav';
  $menu_filter['li_class'] = 'rd-nav-item';
  $menu_filter['a_class'] = 'rd-nav-link';
  $menu_filter['ul_class_deep'] = 'rd-menu rd-navbar-dropdown';
  $menu_filter['li_class_deep'] = 'rd-dropdown-item';
  $menu_filter['li_a_class_deep'] = 'rd-dropdown-link';
  $mt =  menu_tree($menu_filter);
  if($mt != false){
  	print ($mt);
  }
  else {
  	print lnotif(_e('There are no items in the menu', true) . " <b>".$params['menu-name']. '</b>');
  }
?>
