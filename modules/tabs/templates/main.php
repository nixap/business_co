<?php
if ($json == false) {
    print lnotif(_e('Click to edit tabs', true));

    return;
}

if (isset($json) == false or count($json) == 0) {
    $json = array(0 => $defaults);
}
?>
<style>
    .tab-content .element {
        min-height: 0;
    }
</style>


<section class="section section-sm bg-default text-md-left" >
    <div class="container">
        <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
            <div class="col-lg-6 col-xl-5 wow fadeInLeft">
                <h3 class="nodrop safe-mode edit" field="tabs-title-<?php print $params['id']; ?>" rel="tabs-title-<?php print $params['id']; ?>">Cum lucrăm?</h3>
                <!-- Bootstrap tabs-->
                <div class="tabs-custom tabs-horizontal tabs-line tabs-line-big text-center text-md-left">
                    <!-- Nav tabs-->
                    <ul class="nav nav-tabs nodrop safe-mode">
                        <?php
                        $count = 0;
                        foreach ($json as $slide) {
                            $count ++;
                            ?>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link nav-link-big <?php if ($count == 1) { ?> active <?php } ?>" href="#tab-item-<?php print $slide['id']; ?>" data-toggle="tab"><?php print isset($slide['title']) ? $slide['title'] : ''; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                    <!-- Tab panes-->
                    <div class="tab-content">

                        <?php
                        $count = 0;
                        foreach ($json as $key => $slide) {
                            $count ++;
                            $edit_field_key = $key;
                            if (isset($slide['id'])) {
                                $edit_field_key = $slide['id'];
                            }
                            ?>
                            <div class="tab-pane fade show <?php if ($count != 1) { ?>  <?php } else { ?>active <?php } ?> edit allow-drop" field="tab-item-<?php print $edit_field_key ?>" rel="module-t-<?php print $edit_field_key ?>" id="tab-item-<?php print $edit_field_key ?>">
                                <h5 class="font-weight-normal">Tab titlu</h5>
                                <p><?php print isset($slide['content']) ? $slide['content'] : 'Conținut tab ' . $count  ?></p>
                            </div>

                        <?php } ?>

                        <div class="group-md group-middle"><a class="button button-primary button-pipaluk" href="/contact">Contact</a><a class="button button-default-outline button-wapasha" href="#">Descarcă prezentare</a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 text-center wow fadeInUp" data-wow-delay=".1s">
                <div class="figure-classic figure-classic-pattern figure-classic-right nodrop safe-mode edit" field="tabs-img-<?php print $params['id']; ?>" rel="tabs-img-<?php print $params['id']; ?>"><img src="<?php print TEMPLATE_URL; ?>images/index-2-539x434.jpg" alt="" width="539" height="434"/>
                </div>
            </div>
        </div>
    </div>
</section>