<?php
if (isset($slide['primaryText'])) {
    $primaryText = $slide['primaryText'];
}
if (isset($slide['url'])) {
    $url = $slide['url'];
} else {
    $url = 'javascript:;';
}
?>




<div class="swiper-slide context-dark" data-slide-bg="<?php print thumbnail($slide['images'][0], 1920); ?>">
    <div class="swiper-slide-caption section-md">
        <div class="container">
            <h1 data-caption-animate="fadeInLeft" data-caption-delay="0"><?php echo $primaryText; ?></h1>
            <p class="text-width-large" data-caption-animate="fadeInRight" data-caption-delay="100"><?php print $slide['secondaryText']; ?></p>
            <?php if ($slide['seemoreText']): ?>
                <a class="button button-primary button-ujarak" href="<?php
                if (isset($slide['url'])) {
                    print $slide['url'];
                }
                ?>" data-caption-animate="fadeInUp" data-caption-delay="200"><?php print $slide['seemoreText'] ?></a>
               <?php else: ?>
                <a class="button button-primary button-ujarak" href="<?php
                if (isset($slide['url'])) {
                    print $slide['url'];
                }
                ?>" data-caption-animate="fadeInUp" data-caption-delay="200">Detalii</a>


            <?php endif; ?>
        </div>
    </div>
</div>