<?php
/*

  type: layout
  content_type: static
  name: Home
  position: 11
  description: Home layout

 */
?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>


<div class="edit" field="layout-home32" rel="module">
    

    <module type="layouts" rel="home-slide" template="slide" />
    <module type="layouts" rel="home-services" template="services" />
    <module type="layouts" rel="home-about" template="counters" name="about_module" />
    <module type="layouts" rel="home-projects" template="projects"  />
    <module type="layouts" rel="home-how" template="how" name="how_works" />
    <module type="layouts" rel="home-blog" template="hblog"  />
</div>
    

<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
